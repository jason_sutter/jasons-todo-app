import { Component, OnInit, Input } from '@angular/core';
import { todos } from '../todos';
import {Todo} from '../app.component';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  @Input() todos: Todo[];

  constructor() { }

  ngOnInit() {
  }

  removeTodo(todo) {
    const index = todos.findIndex(item => item.id === todo)
    todos.splice(index, 1);
  }
}
