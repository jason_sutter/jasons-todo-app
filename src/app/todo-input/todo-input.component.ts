import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss']
})
export class TodoInputComponent implements OnInit {
  todoState = '';
  @Output() todo = new EventEmitter();

  constructor() { }
  ngOnInit() {
  }

  pushToTodos() {
    this.todo.emit(this.todoState);
  }
}
