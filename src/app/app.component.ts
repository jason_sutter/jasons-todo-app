import { Component } from '@angular/core';
import { todos } from './todos';

export interface Todo {
  todo: string;
  id: number;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'todo-app';
  todos = todos;

  newTodo(inputTodo: Todo) {
    let newID = todos.length + 1;
    todos.push({id: newID,  todo: inputTodo});
  }
}
