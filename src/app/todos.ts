export const todos = [
  {
    id: 10,
    todo: 'learn Angular'
  },
  {
    id: 20,
    todo: 'learn about skinning DNN9 projects'
  },
  {
    id: 30,
    todo: 'have a taco for lunch'
  }
];
